---
title: Badges & Profiles
---

We can use the verification process for links which already exists in Mastodon as a way of safely identifying instances. In short we would provide a profile for each member of an instance that is part of the UFoI which when added to their user profile will show with a green verified checkmark. All users must apply for a profile to get one, but if your instance is in the UFoI your application is garunteed. All UFoI members can apply for a profile, [see the page on applying](/docs/apply-info) for more details.



