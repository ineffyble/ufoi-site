---
title: Getting Started
---

The Fediverse derives strength from its decentralized, multi-instance structure. Localized administration allows each instance to implement a moderation policy to promote productive interactions among its users while federating with other instances to expand the scope of who its users can interact with. However, as the fediverse grows it is increasingly difficult for instance administrators to investigate whether each other instance maintains moderation practices that are compatible with the aims of their own instance. As a result, instancelevel blocks may be implemnted due to miscommunication, rumors propagated without context, or even malicious attempts to undermine the Fediverse as a whole. Such blocking is highly disruptive to the fediverse, as it can isolate groups of tens of thousands (or more) of users from each other.

The UFoI is an alliance of Fediverse instance; member instances adhere to a set of articles that form a [Code of Ethics](/docs/code-of-ethics).

The United Federation of Instances aims to maximize the ability of any two people to communicate using the Fediverse by establishing basic moderation standards that minimizes harmful content and provides confilct-resolution mechanisms to prevent members instances from blocking each other. Member instances would adhere to a set of articles that form a code-of-ethics. The articles are designed so that instances retain their autonomy, ensuring users continue to receive the benefits of the decentralized fediverse structure, while at the same time protecting them from the disruptive effects of instance-fracturing attacks.

# The Problem

There is a problem in the Fediverse that has been growing for some time, a fracturing which is ultimately undermining the federated and decentralized nature of the Fediverse. Instance owners are defederating from other instances based largely on gossip and unfounded accusations without there being any due process or evidence gathering to ensure well informed decisions are being made. To make matters worse these instances are demanding other instances defederate from all the instances they passed summary judgement on lest be added to the suspend list with the others.

To add insult to injury this has been known to be exploited by bad actors. In one such case[1] a known and self-described Nazi went around posing as LGBTQ members on various servers and used this to spread misinformation, and the campaign has largely been successful resulting in further fracturing. The proposal made here is intended to be a social, rather than technological, solution to this problem. It will ensure accusations are processed in a way that evaluates the evidence, allows both sides to make their argument, and concludes with a democratic process to make a decision. Known good actors are welcome in the federation, bad ones arent, accusations must be proven. Those who petition to be part of the federation likewise agree not to defederate from instances within the federation and must instead provide the evidence, and have the decision to kick someone from the federation come down to a democratic due process. The goal is to create a unified Fediverse of good actor instances and put a stop to the fracturing that will ultimately destroy the entire network.

The problem lies largely in the way defederation considerations are evaluated in the Fediverse among many instances, it isn't based on facts or due process but simply hearsay. As we all know lies spread faster than truth so this is a troubling pattern. What amplifies the problem tremendously is those who block an instance often demand every instance they federate block the same instances as them, if they do not then they get added to the suspend list as well. This leads to a severe fracturing of the Fediverse that is quickly devolving into walled gardens that resemble the data silos of Facebook and Twitter. We can do better, we must do better.

The problem is effectively one of an absence of due process, evidence isn't presented, the other side isn't heard, decisions are final. People sometimes see this as verification that a person or instance is a bad actor amongst the rumours they have heard and these decisions ripple outwards. In one such case[1] a known and self-proclaimed Nazi recognized this pattern and exploited it to manipulate the LGBTQ community against itself causing the aggressive opposition to others within their own community.

The solution, therefore, is to ensure due process, enable discussion and to do so in a transparent fashion visible to all parties and with all sides having a chance to present evidence. While good-actor instances shall remain unified and federated amongst each other, any instances that have not shown to be good-actors by joining the United federation of Instances (UFI) are not under any special protections, they should be allowed to be moderated by instances however an instance sees fit. However non-UFI members should not be assumed to be bad actors either, simply not being a member of the UFI should not imply a certain prejudice on its own.

[1]: https://jeffreyfreeman.me/eugen-rochko-ceo-of-mastodon-found-to-support-nazis-agenda/

# How to Help

The best way to help is to convince your instance admin to join the UFoI. Talk to them, and if they are interested share with them the [applying](/docs/apply-info) page so they can get your instance in the UFoI.

You can also join the UFoI as an individual member if you pledge to uphold our [Code of Ethics](/docs/code-of-ethics). You can also see the [applying](/docs/apply-info) page for more information on that.

Finally you can always contribute either as a member or not. At the very least you can join the conversation and listen or provide feedback. To get involved see our [getting involved](/docs/getting-involved) page.
