---
title: Right to Leave
---

Any instance that is a member of the UFoI is free to leave the federation at any time without any prior notice or approval needed. The admin of the instance must, however, publicly inform the UFI Council they are leaving. Any instance that leaves the UFoI must go through the entire admittance process over should they wish to rejoin.

The freedom to leave is one of the fundamental protections of the UFoI, if all else fails people can always leave and potentially form their own federation over, addressing any problems they felt the old system had. In some cases this cause federations to split into two distinctly separate federations. This should hopefully improve initial adoption since entry into the UFoI is effectively non-binding since anyone can leave at any moment.


