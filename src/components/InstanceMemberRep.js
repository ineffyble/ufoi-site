import {
  Button,
  Card,
  Heading1,
  InlineLink,
  Input,
  LabelText,
  Text,
} from '@algolia/ui-library';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import React, { useState } from 'react';
import instanceMembers from './instance-members.json';
import userMembers from './user-members.json';

function InstanceMemberRep() {
  const { withBaseUrl } = useBaseUrlUtils();

  return (
    <div id="tailwind">
      <div className="py-16 overflow-hidden">
        <div className="relative max-w-xl mx-auto px-4 md:px-6 lg:px-8 lg:max-w-screen-xl">
          <div className="max-w-screen-xl mx-auto px-4 md:px-6 lg:px-8">
              <div className="col-span-1 flex justify-center py-2 px-2 text-center">
                <h1>Instance Member Representatives</h1>
              </div>
              {userMembers.map(({ name, display_name, profiles, instance_member_reps, on_council, on_ufoi_instance }) => instance_member_reps && (
                <div
                  key={name}
                >
                  {instance_member_reps.map(instance_member_rep => (
                    <div key={instance_member_rep} className="col-span-1 flex justify-left py-2 px-2 text-center">
                      {instance_member_rep}: <a href={withBaseUrl('/u/' + name)} alt={'UFoI user profile for ' + name}>/u/{name}</a>
                    </div>
                  ))}

                </div>
              )
              )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default InstanceMemberRep;
