import { Hero } from '@algolia/ui-library';
import Layout from '@theme/Layout';
import React from 'react';

import CouncilMembers from '../components/CouncilMembers.js';
import SiteLogo from '../components/SiteLogo';

function CouncilMembersPage() {
  return (
    <Layout
      title="UFoI: Instance Member Representatives"
      description="A federation of good-faith actors on the Fediverse"
    >
      <div className="uil-pb-24">
        <Hero
          id="council-member"
          title={<SiteLogo width="100%" />}
          background="curves"
        />
        <CouncilMembers />
      </div>
    </Layout>
  );
}

export default CouncilMembersPage;
